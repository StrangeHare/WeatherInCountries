package com.test.weatherincountries

import android.app.Application
import com.test.weatherincountries.di.component.AppComponent
import com.test.weatherincountries.di.component.DaggerAppComponent
import com.test.weatherincountries.di.module.AppModule
import io.realm.Realm
import io.realm.RealmConfiguration

/**
 *
 * @author StrangeHare
 *         Date: 09.10.2017
 */
class App : Application() {

    companion object {
        lateinit var realm: Realm
    }

    lateinit var appComponent: AppComponent

    override fun onCreate() {
        super.onCreate()
        Realm.init(this)
        Realm.setDefaultConfiguration(RealmConfiguration.Builder().build())

        appComponent = DaggerAppComponent.builder().appModule(AppModule(this)).build()
        appComponent.inject(this)
        realm = appComponent.getRealm()
    }

    override fun onTerminate() {
        realm.close()
        super.onTerminate()
    }
}