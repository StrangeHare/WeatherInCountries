package com.test.weatherincountries.di.component

import com.test.weatherincountries.di.module.ActivityModule
import com.test.weatherincountries.di.scope.ActivityScope
import dagger.Component

/**
 *
 * @author StrangeHare
 * Date: 09.10.2017
 */
@ActivityScope
@Component(dependencies = arrayOf(AppComponent::class), modules = arrayOf(ActivityModule::class))
interface ActivityComponent