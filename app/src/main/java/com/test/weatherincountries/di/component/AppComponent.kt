package com.test.weatherincountries.di.component

import android.app.Application
import android.content.Context
import com.test.weatherincountries.App
import com.test.weatherincountries.di.module.AppModule
import com.test.weatherincountries.di.qualifier.ApplicationContextQualifier
import dagger.Component
import io.realm.Realm
import javax.inject.Singleton

/**
 *
 * @author StrangeHare
 *         Date: 09.10.2017
 */
@Singleton
@Component(modules = arrayOf(AppModule::class))
interface AppComponent {

    fun inject(app: App)

    @ApplicationContextQualifier
    fun getContext(): Context

    fun getApplication(): Application

    fun getRealm(): Realm
}