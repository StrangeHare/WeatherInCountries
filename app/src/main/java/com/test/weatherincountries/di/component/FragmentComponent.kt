package com.test.weatherincountries.di.component

import com.test.weatherincountries.di.module.FragmentModule
import com.test.weatherincountries.di.scope.FragmentScope
import com.test.weatherincountries.ui.details.FragmentDetails
import com.test.weatherincountries.ui.main.FragmentMain

import dagger.Component

/**
 * Компонент фрагмента
 * @author kurbatov_s
 * Date: 30.08.17
 */
@FragmentScope
@Component(dependencies = arrayOf(ActivityComponent::class), modules = arrayOf(FragmentModule::class))
interface FragmentComponent {

    fun injectFragmentMain(fragment: FragmentMain)

    fun injectFragmentDetails(fragment: FragmentDetails)
}
