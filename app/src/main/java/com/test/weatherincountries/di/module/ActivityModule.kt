package com.test.weatherincountries.di.module

import android.content.Context
import android.support.v7.app.AppCompatActivity
import com.test.weatherincountries.di.qualifier.ActivityContextQualifier
import com.test.weatherincountries.di.scope.ActivityScope
import com.test.weatherincountries.mvp.presenter.DetailsPresenter
import com.test.weatherincountries.mvp.presenter.IDetailsPresenter
import com.test.weatherincountries.mvp.presenter.IMainPresenter
import com.test.weatherincountries.mvp.presenter.MainPresenter
import com.test.weatherincountries.mvp.view.IDetailsView
import com.test.weatherincountries.mvp.view.IMainView
import dagger.Module
import dagger.Provides

/**
 *
 * @author StrangeHare
 *         Date: 09.10.2017
 */
@Module
class ActivityModule(private var activity: AppCompatActivity) {

    @Provides
    @ActivityContextQualifier
    fun provideContext(): Context = activity

    @Provides
    fun provideActivity(): AppCompatActivity = activity

    @Provides
    @ActivityScope
    fun provideDetailsPresenter(presenter: DetailsPresenter<IDetailsView>): IDetailsPresenter<IDetailsView> = presenter

    @Provides
    @ActivityScope
    fun provideMainPresenter(presenter: MainPresenter<IMainView>): IMainPresenter<IMainView> = presenter
}
