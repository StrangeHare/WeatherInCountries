package com.test.weatherincountries.di.module

import android.app.Application
import android.content.Context
import com.test.weatherincountries.di.qualifier.ApplicationContextQualifier
import dagger.Module
import dagger.Provides
import io.realm.Realm
import javax.inject.Singleton

/**
 *
 * @author StrangeHare
 *         Date: 09.10.2017
 */
@Module
class AppModule(var app: Application) {

    @Provides
    @Singleton
    @ApplicationContextQualifier
    fun provideContext(): Context = app

    @Provides
    fun provideApplication(): Application = app

    @Provides
    @Singleton
    fun provideRealm() = Realm.getDefaultInstance()!!
}