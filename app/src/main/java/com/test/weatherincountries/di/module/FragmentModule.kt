package com.test.weatherincountries.di.module

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import com.test.weatherincountries.di.qualifier.ChildFragmentManager
import com.test.weatherincountries.di.scope.FragmentScope
import com.test.weatherincountries.mvp.presenter.DetailsPresenter
import com.test.weatherincountries.mvp.presenter.IDetailsPresenter
import com.test.weatherincountries.mvp.presenter.IMainPresenter
import com.test.weatherincountries.mvp.presenter.MainPresenter
import com.test.weatherincountries.mvp.view.IDetailsView
import com.test.weatherincountries.mvp.view.IMainView
import dagger.Module
import dagger.Provides

/**
 *
 * @author StrangeHare
 * Date: 09.10.2017
 */
@Module
class FragmentModule(private val fragment: Fragment) {

    @Provides
    @FragmentScope
    @ChildFragmentManager
    internal fun provideChildFragmentManager(): FragmentManager = fragment.childFragmentManager

    @Provides
    @FragmentScope
    fun provideDetailsPresenter(presenter: DetailsPresenter<IDetailsView>): IDetailsPresenter<IDetailsView> = presenter

    @Provides
    @FragmentScope
    fun provideMainPresenter(presenter: MainPresenter<IMainView>): IMainPresenter<IMainView> = presenter
}
