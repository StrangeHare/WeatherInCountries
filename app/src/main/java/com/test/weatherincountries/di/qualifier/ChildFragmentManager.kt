package com.test.weatherincountries.di.qualifier

import javax.inject.Qualifier

/**
 *
 * @author StrangeHare
 *         Date: 09.10.2017
 */
@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class ChildFragmentManager
