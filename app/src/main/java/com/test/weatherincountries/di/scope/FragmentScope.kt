package com.test.weatherincountries.di.scope

import javax.inject.Scope

/**
 *
 * @author StrangeHare
 *         Date: 09.10.2017
 */
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class FragmentScope
