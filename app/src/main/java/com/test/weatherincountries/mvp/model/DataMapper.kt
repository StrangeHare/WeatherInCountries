package com.test.weatherincountries.mvp.model

import com.test.weatherincountries.utils.FormatUtils
import java.util.*

/**
 *
 * @author StrangeHare
 *         Date: 23.10.2017
 */
class DataMapper {

    companion object {

        fun mapData(data: List<WeatherDay>?, callback: MapperCallback) {
            val listFirstDay: LinkedList<WeatherDay> = LinkedList()
            val listSecondDay: LinkedList<WeatherDay> = LinkedList()
            val listThirdDay: LinkedList<WeatherDay> = LinkedList()
            val listFourDay: LinkedList<WeatherDay> = LinkedList()
            val listFiveDay: LinkedList<WeatherDay> = LinkedList()

            val firstDay = FormatUtils.format(data!![0].timestamp!!)
            var secondDay: String? = null
            var thirdDay: String? = null
            var fourthDay: String? = null
            var fiveDay: String? = null

            data.sortedBy { it.timestamp }
            for (item in data) {
                if (firstDay != FormatUtils.format(item.timestamp!!)) {
                    if (secondDay == null) {
                        secondDay = FormatUtils.format(item.timestamp!!)
                    }
                    if (secondDay != FormatUtils.format(item.timestamp!!) && thirdDay == null) {
                        thirdDay = FormatUtils.format(item.timestamp!!)
                    }
                    if (thirdDay != null && thirdDay != FormatUtils.format(item.timestamp!!) && fourthDay == null) {
                        fourthDay = FormatUtils.format(item.timestamp!!)
                    }
                    if (fourthDay != null && fourthDay != FormatUtils.format(item.timestamp!!) && fiveDay == null) {
                        fiveDay = FormatUtils.format(item.timestamp!!)
                    }
                }
            }

            for (item in data) {
                when (FormatUtils.format(item.timestamp!!)) {
                    firstDay -> listFirstDay.add(item)
                    secondDay -> listSecondDay.add(item)
                    thirdDay -> listThirdDay.add(item)
                    fourthDay -> listFourDay.add(item)
                    fiveDay -> listFiveDay.add(item)
                }
            }

            callback.onMapped(listFirstDay, listSecondDay, listThirdDay, listFourDay, listFiveDay)
        }
    }

    interface MapperCallback {

        fun onMapped(listFirstDay: LinkedList<WeatherDay>,
                     listSecondDay: LinkedList<WeatherDay>,
                     listThirdDay: LinkedList<WeatherDay>,
                     listFourDay: LinkedList<WeatherDay>,
                     listFiveDay: LinkedList<WeatherDay>)
    }
}