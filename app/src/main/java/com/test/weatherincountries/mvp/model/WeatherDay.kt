package com.test.weatherincountries.mvp.model

import com.google.gson.annotations.SerializedName
import com.test.weatherincountries.network.ApiWeather.WeatherApiInterface.Companion.ICON_EXPANSION
import com.test.weatherincountries.network.ApiWeather.WeatherApiInterface.Companion.ICON_URL
import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

/**
 *
 * @author StrangeHare
 *         Date: 16.10.2017
 */
open class WeatherDay: RealmObject(), Comparable<WeatherDay> {

    @SerializedName("main")
    var temp: WeatherTemp? = null

    @SerializedName("weather")
    var description: RealmList<WeatherDescription>? = null

    @SerializedName("id")
    var id: Long? = 0

    @PrimaryKey
    @SerializedName("name")
    var city: String? = null

    @SerializedName("dt")
    var timestamp: Long? = 0

    var tempWithDegree: String? = null
        get() = temp?.temp?.toInt().toString() + "\u00B0"

    var icon: String? = null
        get() = description!![0].icon

    var iconUrl: String? = null
        get() = ICON_URL + icon + ICON_EXPANSION

    override fun compareTo(other: WeatherDay): Int = city?.compareTo(other.city!!)!!
}