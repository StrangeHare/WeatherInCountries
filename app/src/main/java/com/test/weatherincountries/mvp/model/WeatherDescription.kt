package com.test.weatherincountries.mvp.model

import io.realm.RealmObject

/**
 *
 * @author StrangeHare
 *         Date: 24.10.2017
 */
open class WeatherDescription: RealmObject(){
    var icon: String? = null
}