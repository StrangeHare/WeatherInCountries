package com.test.weatherincountries.mvp.model

import com.google.gson.annotations.SerializedName
import io.realm.RealmList
import io.realm.RealmObject

/**
 *
 * @author StrangeHare
 *         Date: 16.10.2017
 */
open class WeatherForecast constructor(
        @field:SerializedName("list")
        var items: RealmList<WeatherDay>? = null
): RealmObject()