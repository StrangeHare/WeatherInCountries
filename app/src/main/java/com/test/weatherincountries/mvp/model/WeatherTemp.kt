package com.test.weatherincountries.mvp.model

import io.realm.RealmObject

/**
 *
 * @author StrangeHare
 *         Date: 24.10.2017
 */
open class WeatherTemp: RealmObject() {
    var id: Long? = null
    var temp: Double? = null
}