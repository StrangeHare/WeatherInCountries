package com.test.weatherincountries.mvp.presenter

import com.test.weatherincountries.mvp.view.IView
import javax.inject.Inject

/**
 *
 * @author StrangeHare
 *         Date: 09.10.2017
 */
open class BasePresenter<V : IView> @Inject constructor() : IPresenter<V> {

    var iView: V? = null

    override fun onAttach(view: V) {
        this.iView = view
    }

    override fun onDetach() {
        iView = null
    }

    fun isViewAttached(): Boolean = iView != null
}