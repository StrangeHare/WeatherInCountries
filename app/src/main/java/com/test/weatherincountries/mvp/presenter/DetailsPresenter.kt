package com.test.weatherincountries.mvp.presenter

import com.test.weatherincountries.mvp.model.DataMapper
import com.test.weatherincountries.mvp.model.WeatherDay
import com.test.weatherincountries.mvp.view.IDetailsView
import com.test.weatherincountries.network.ApiProvider
import com.test.weatherincountries.network.IProviderForSevenDay
import com.test.weatherincountries.network.IWeatherRequestCallback
import java.util.*
import javax.inject.Inject

/**
 *
 * @author StrangeHare
 *         Date: 09.10.2017
 */
class DetailsPresenter<V : IDetailsView> @Inject constructor() : BasePresenter<V>(), IDetailsPresenter<V>, IWeatherRequestCallback, DataMapper.MapperCallback {

    var provider: IProviderForSevenDay = ApiProvider(this)
    lateinit var name: String

    override fun onAttach(view: V) {
        super.onAttach(view)
        provider.doRequestForSevenDayWeather(name)
    }

    override fun onDetach() {
        super.onDetach()
        provider.presenterIsDestroy()
    }

    override fun setCityName(cityName: String) {
        name = cityName
    }

    override fun onMapped(listFirstDay: LinkedList<WeatherDay>, listSecondDay: LinkedList<WeatherDay>, listThirdDay: LinkedList<WeatherDay>, listFourDay: LinkedList<WeatherDay>, listFiveDay: LinkedList<WeatherDay>) {
        iView?.onWeatherObtained(listFirstDay, listSecondDay, listThirdDay, listFourDay, listFiveDay)
    }

    override fun requestSuccess(items: List<WeatherDay>?) {
        DataMapper.mapData(items, this)
    }

    override fun requestFailed(t: Throwable) {
        iView?.showError(t.localizedMessage)
    }
}