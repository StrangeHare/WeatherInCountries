package com.test.weatherincountries.mvp.presenter

import com.test.weatherincountries.di.scope.ActivityScope
import com.test.weatherincountries.mvp.view.IDetailsView

/**
 *
 * @author StrangeHare
 *         Date: 09.10.2017
 */
@ActivityScope
interface IDetailsPresenter<V : IDetailsView> : IPresenter<V> {

    fun setCityName(cityName: String)
}