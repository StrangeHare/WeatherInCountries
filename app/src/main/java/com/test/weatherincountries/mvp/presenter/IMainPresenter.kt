package com.test.weatherincountries.mvp.presenter

import com.test.weatherincountries.di.scope.ActivityScope
import com.test.weatherincountries.mvp.view.IMainView
import com.test.weatherincountries.ui.adapter.AdapterWeather

/**
 *
 * @author StrangeHare
 *         Date: 09.10.2017
 */
@ActivityScope
interface IMainPresenter<V : IMainView> : IPresenter<V> {

    fun getCurrentAdapter(): AdapterWeather

    fun filterQuery(s: String?)
}