package com.test.weatherincountries.mvp.presenter

import com.test.weatherincountries.mvp.view.IView


/**
 *
 * @author StrangeHare
 *         Date: 09.10.2017
 */
interface IPresenter<V : IView> {

    fun onAttach(view: V)

    fun onDetach()
}