package com.test.weatherincountries.mvp.presenter

import android.text.TextUtils
import com.test.weatherincountries.R
import com.test.weatherincountries.mvp.model.WeatherDay
import com.test.weatherincountries.mvp.view.IMainView
import com.test.weatherincountries.network.ApiProvider
import com.test.weatherincountries.network.IProviderWeatherDay
import com.test.weatherincountries.network.IWeatherRequestCallback
import com.test.weatherincountries.ui.adapter.AdapterWeather
import com.test.weatherincountries.utils.NetworkUtils
import com.test.weatherincountries.utils.TimerUtils
import javax.inject.Inject

/**
 *
 * @author StrangeHare
 *         Date: 09.10.2017
 */
class MainPresenter<V : IMainView> @Inject constructor() : BasePresenter<V>(), IMainPresenter<V>, IWeatherRequestCallback, TimerUtils.TimerFinish<V> {

    var providerWeatherDay: IProviderWeatherDay = ApiProvider(this)
    private var timer: TimerUtils<V> = TimerUtils()
    @Inject
    lateinit var adapterWeather: AdapterWeather

    override fun onAttach(view: V) {
        super.onAttach(view)
        getWeather(view)
        timer.attachView(view)
        timer.initTimer(this)
    }

    override fun onDetach() {
        super.onDetach()
        providerWeatherDay.presenterIsDestroy()
        timer.stopTimer()
    }

    override fun getCurrentAdapter(): AdapterWeather = adapterWeather

    override fun filterQuery(s: String?) {
        if (TextUtils.isEmpty(s)) adapterWeather.filter.filter("") else adapterWeather.filter.filter(s.toString())
    }

    override fun requestSuccess(items: List<WeatherDay>?) {
        adapterWeather.addData(providerWeatherDay.findInRealmAll())
        timer.startTimer()
        iView?.onWeatherObtained()
    }

    override fun requestFailed(t: Throwable) {
        iView?.showError(t.localizedMessage)
    }

    override fun onFinish(view: V) {
        getWeather(view)
    }

    private fun getWeather(view: V) {
        if (NetworkUtils.isNetworkConnected(view.getContext())) {
            providerWeatherDay.doRequestWeatherDay()
        } else {
            adapterWeather.addData(providerWeatherDay.findInRealmAll())
            iView?.showError(view.getContext().getString(R.string.no_internet))
        }
    }
}