package com.test.weatherincountries.mvp.view

import com.test.weatherincountries.mvp.model.WeatherDay
import java.util.*

/**
 *
 * @author StrangeHare
 *         Date: 09.10.2017
 */
interface IDetailsView : IView {

    fun showError(error: String?)

    fun onWeatherObtained(listFirstDay: LinkedList<WeatherDay>,
                          listSecondDay: LinkedList<WeatherDay>,
                          listThirdDay: LinkedList<WeatherDay>,
                          listFourDay: LinkedList<WeatherDay>,
                          listFiveDay: LinkedList<WeatherDay>)
}