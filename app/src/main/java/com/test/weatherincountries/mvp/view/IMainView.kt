package com.test.weatherincountries.mvp.view

import android.content.Context

/**
 *
 * @author StrangeHare
 *         Date: 09.10.2017
 */
interface IMainView : IView {

    fun showError(error: String?)

    fun onWeatherObtained()

    fun getContext(): Context
}