package com.test.weatherincountries.mvp.view

/**
 *
 * @author StrangeHare
 *         Date: 09.10.2017
 */
interface IView {

    fun showLoading()

    fun hideLoading()

    fun showMessage(message: String)
}