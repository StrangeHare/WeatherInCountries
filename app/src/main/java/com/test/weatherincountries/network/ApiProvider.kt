package com.test.weatherincountries.network

import android.text.TextUtils
import android.util.Log
import com.test.weatherincountries.mvp.model.WeatherDay
import com.test.weatherincountries.mvp.model.WeatherForecast
import com.test.weatherincountries.mvp.model.WeatherTemp
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import io.realm.Realm
import io.realm.RealmResults
import javax.inject.Inject

/**
 *
 * @author StrangeHare
 *         Date: 19.10.2017
 */
class ApiProvider @Inject constructor(private val iteractable: IWeatherRequestCallback): IProviderWeatherDay, IProviderForSevenDay {

    private val disposables: CompositeDisposable = CompositeDisposable()

    private fun findInRealm(realm: Realm, city: String?): WeatherDay? {
        if (city == null) {
            return null
        }

        return realm.where(WeatherDay::class.java).equalTo("city", city).findFirst()
    }

    override fun findInRealmAll(): RealmResults<WeatherDay> = Realm.getDefaultInstance().where(WeatherDay::class.java).findAll()

    private fun writeToRealm(response: List<WeatherDay>?) {
        val realm = Realm.getDefaultInstance()
        realm.executeTransaction { transactionRealm ->
            for (item in response!!) {
                var weatherRealm: WeatherDay? = findInRealm(transactionRealm, item.city)
                if (weatherRealm == null) {
                    weatherRealm = transactionRealm.createObject(WeatherDay::class.java, item.city)
                }
                weatherRealm?.temp = transactionRealm.copyToRealm(item.temp)
                weatherRealm?.description?.addAll(transactionRealm.copyToRealm(item.description))
                weatherRealm?.id = item.id
                weatherRealm?.timestamp = item.timestamp
                weatherRealm?.tempWithDegree = item.tempWithDegree
                weatherRealm?.icon = item.icon
                weatherRealm?.iconUrl = item.iconUrl
            }
        }
        realm.close()
    }

    override fun doRequestWeatherDay() {
        disposables.add(ApiWeather.getWeatherByBox()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ t: WeatherForecast ->
                    val items: List<WeatherDay>? = t.items?.filter { !TextUtils.isEmpty(it.city) }?.distinctBy { it.city }?.sorted()
                    writeToRealm(items)
                    iteractable.requestSuccess(t.items)
                }, { t: Throwable ->
                    iteractable.requestFailed(t)
                }))
    }

    override fun doRequestForSevenDayWeather(name: String) {
        disposables.add(ApiWeather.getWeatherForSevenDay(name)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ t: WeatherForecast ->
                    iteractable.requestSuccess(t.items)
                }, { t: Throwable ->
                    iteractable.requestFailed(t)
                }))
    }

    override fun presenterIsDestroy() {
        disposables.dispose()
    }
}