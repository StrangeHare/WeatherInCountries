package com.test.weatherincountries.network

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import com.test.weatherincountries.mvp.model.WeatherDay
import com.test.weatherincountries.mvp.model.WeatherForecast
import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

/**
 *
 * @author StrangeHare
 *         Date: 09.10.2017
 */
object ApiWeather {

    private var weatherApi: WeatherApiInterface

    init {
        val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(WeatherApiInterface.BASE_URL)
                .build()
        weatherApi = retrofit.create<WeatherApiInterface>(WeatherApiInterface::class.java)
    }

    fun getTodayWeather(cityName: String): Observable<WeatherDay> =
            weatherApi.getTodayWeather(cityName, WeatherApiInterface.UNITS_METRIC, WeatherApiInterface.API_KEY)

    fun getWeatherForSevenDay(cityName: String): Observable<WeatherForecast> =
            weatherApi.getWeatherForSevenDay(cityName, WeatherApiInterface.UNITS_METRIC, WeatherApiInterface.CNT, WeatherApiInterface.API_KEY)

    fun getWeatherByBox(): Observable<WeatherForecast> =
            weatherApi.getWeatherByBox(WeatherApiInterface.BBOX, WeatherApiInterface.UNITS_METRIC, WeatherApiInterface.API_KEY)

    interface WeatherApiInterface {

        companion object {
            val API_KEY: String
                get() = "d6eec258e6003a1ab46d2f3d32dfeeca"

            val BASE_URL: String
                get() = "http://api.openweathermap.org/data/2.5/"

            val ICON_URL: String
                get() = "http://openweathermap.org/img/w/"

            val ICON_EXPANSION: String
                get() = ".png"

            val UNITS_METRIC: String
                get() = "metric"

            val CNT: Int
                get() = 40

            val BBOX: String
                get() = "-16,30,69,71,$ZOOM"

            val ZOOM: String
                get() = "10"
        }

        @GET("weather?")
        fun getTodayWeather(@Query("q") q: String,
                            @Query("units") units: String,
                            @Query("appid") appid: String): Observable<WeatherDay>

        @GET("forecast?")
        fun getWeatherForSevenDay(@Query("q") q: String,
                                  @Query("units") units: String,
                                  @Query("cnt") cnt: Int,
                                  @Query("appid") appid: String): Observable<WeatherForecast>

        @GET("box/city?")
        fun getWeatherByBox(@Query("bbox") bbox: String,
                            @Query("units") units: String,
                            @Query("appid") appid: String): Observable<WeatherForecast>
    }
}