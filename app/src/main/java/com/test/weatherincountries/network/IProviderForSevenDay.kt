package com.test.weatherincountries.network

/**
 *
 * @author StrangeHare
 *         Date: 23.10.2017
 */
interface IProviderForSevenDay: IProvider {

    fun doRequestForSevenDayWeather(name: String)
}