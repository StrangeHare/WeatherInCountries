package com.test.weatherincountries.network

import com.test.weatherincountries.mvp.model.WeatherDay
import io.realm.RealmResults

/**
 *
 * @author StrangeHare
 *         Date: 23.10.2017
 */
interface IProviderWeatherDay: IProvider {

    fun doRequestWeatherDay()

    fun findInRealmAll(): RealmResults<WeatherDay>
}