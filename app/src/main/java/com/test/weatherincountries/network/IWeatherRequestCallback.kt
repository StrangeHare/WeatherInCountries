package com.test.weatherincountries.network

import com.test.weatherincountries.mvp.model.WeatherDay

/**
 *
 * @author StrangeHare
 *         Date: 23.10.2017
 */
interface IWeatherRequestCallback {

    fun requestSuccess(items: List<WeatherDay>?)

    fun requestFailed(t: Throwable)
}