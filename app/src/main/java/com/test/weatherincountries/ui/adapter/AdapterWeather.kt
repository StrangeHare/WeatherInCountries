package com.test.weatherincountries.ui.adapter

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import com.test.weatherincountries.mvp.model.WeatherDay
import com.test.weatherincountries.ui.adapter.holder.WeatherHolder
import com.test.weatherincountries.ui.adapter.holder.WeatherHolderView
import org.jetbrains.anko.AnkoContext
import java.util.*
import javax.inject.Inject

/**
 *
 * @author StrangeHare
 *         Date: 09.10.2017
 */
class AdapterWeather @Inject constructor() : RecyclerView.Adapter<WeatherHolder>(), Filterable {

    var data: ArrayList<WeatherDay> = ArrayList()
    private var orig: ArrayList<WeatherDay>? = null

    override fun onBindViewHolder(holder: WeatherHolder, position: Int) {
        holder.bind(data[position])
    }

    override fun onCreateViewHolder(view: ViewGroup?, position: Int): WeatherHolder =
            WeatherHolder(WeatherHolderView().createView(AnkoContext.create(view!!.context, view)))

    override fun getItemCount(): Int = data.size

    fun addData(items: List<WeatherDay>?) {
        data.clear()
        data.addAll(items!!)
        notifyDataSetChanged()
    }

    override fun getFilter(): Filter = object : Filter() {

        override fun performFiltering(constraint: CharSequence?): Filter.FilterResults {
            val returnToUi = Filter.FilterResults()
            val results = ArrayList<WeatherDay>()

            if (orig == null) {
                orig = data
            }

            if (constraint != null) {
                if ((orig != null) and (orig?.size!! > 0)) {
                    results.addAll(orig!!)
                }
                returnToUi.values = results
            }

            return returnToUi
        }

        override fun publishResults(constraint: CharSequence, results: Filter.FilterResults) {
            val fromWorkThread = ArrayList<WeatherDay>()

            (results.values as ArrayList<WeatherDay>).filterTo(fromWorkThread) {
                it.city!!.contains(constraint.toString(), true)
            }

            data = fromWorkThread
            notifyDataSetChanged()
        }
    }
}