package com.test.weatherincountries.ui.adapter.holder

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso
import com.test.weatherincountries.R
import com.test.weatherincountries.mvp.model.WeatherDay
import com.test.weatherincountries.ui.details.ActivityDetails
import org.jetbrains.anko.find

/**
 *
 * @author StrangeHare
 *         Date: 20.10.17
 */
class WeatherHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

    private val textViewCity: TextView = itemView.find(R.id.text_view_city)
    private val textViewTemp: TextView = itemView.find(R.id.text_view_temp)
    private val imageVieIcon: ImageView = itemView.find(R.id.image_view_weather)

    init {
        itemView.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        val intent = Intent(v?.context, ActivityDetails::class.java)
        intent.putExtra(v?.context?.getString(R.string.city_name), textViewCity.text)
        v?.context?.startActivity(intent)
    }

    fun bind(data: WeatherDay) {
        textViewCity.text = data.city
        textViewTemp.text = data.tempWithDegree
        Picasso.with(itemView.context).load(data.iconUrl).into(imageVieIcon)
    }
}