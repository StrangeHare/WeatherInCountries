package com.test.weatherincountries.ui.adapter.holder

import android.support.v4.content.ContextCompat
import android.text.TextUtils
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import com.test.weatherincountries.R
import org.jetbrains.anko.*

/**
 *
 * @author StrangeHare
 *         Date: 13.10.17
 */
class WeatherHolderView : AnkoComponent<ViewGroup> {

    override fun createView(ui: AnkoContext<ViewGroup>): View = with(ui) {
            linearLayout {
                background = ContextCompat.getDrawable(context, R.color.background_item)
                lparams(width = matchParent, height = wrapContent)

                imageView {
                    id = R.id.image_view_weather
                    imageResource = R.drawable.ic_template
                }.lparams(width = dip(48), height = dip(48)) {
                    rightMargin = dip(8)
                    gravity = Gravity.CENTER
                }

                verticalLayout {
                    lparams(width = matchParent, height = wrapContent)

                    textView {
                        id = R.id.text_view_city
                        ellipsize = TextUtils.TruncateAt.MIDDLE
                        maxLines = 1
                    }.lparams(width = matchParent, height = wrapContent) {
                        padding = dip(16)
                    }

                    textView {
                        id = R.id.text_view_temp
                    }.lparams(width = matchParent, height = wrapContent) {
                        padding = dip(16)
                    }
                }
            }
        }
}