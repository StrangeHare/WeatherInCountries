package com.test.weatherincountries.ui.base

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.test.weatherincountries.App
import com.test.weatherincountries.di.component.ActivityComponent
import com.test.weatherincountries.di.component.DaggerActivityComponent
import com.test.weatherincountries.di.module.ActivityModule
import com.test.weatherincountries.mvp.view.IView

/**
 *
 * @author StrangeHare
 *         Date: 09.10.2017
 */
open class ActivityBase : AppCompatActivity(), IView {

    lateinit var activityComponent: ActivityComponent

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityComponent = DaggerActivityComponent.builder()
                .appComponent((application as App).appComponent)
                .activityModule(ActivityModule(this))
                .build()
    }

    override fun showLoading() {
    }

    override fun hideLoading() {
    }

    override fun showMessage(message: String) {
    }
}
