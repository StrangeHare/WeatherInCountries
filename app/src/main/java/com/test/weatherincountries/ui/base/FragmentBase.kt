package com.test.weatherincountries.ui.base

import android.os.Bundle
import android.support.v4.app.Fragment
import android.widget.Toast
import com.test.weatherincountries.di.component.DaggerFragmentComponent
import com.test.weatherincountries.di.component.FragmentComponent
import com.test.weatherincountries.di.module.FragmentModule
import com.test.weatherincountries.mvp.view.IView

/**
 *
 * @author StrangeHare
 *         Date: 09.10.2017
 */
open class FragmentBase : Fragment(), IView {

    lateinit var fragmentComponent: FragmentComponent

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        fragmentComponent = DaggerFragmentComponent
                .builder()
                .fragmentModule(FragmentModule(this))
                .activityComponent((activity as ActivityBase).activityComponent)
                .build()
    }

    override fun showLoading() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun hideLoading() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun showMessage(message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}