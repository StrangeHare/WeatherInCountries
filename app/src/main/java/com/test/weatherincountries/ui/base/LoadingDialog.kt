package com.test.weatherincountries.ui.base

import android.app.Dialog
import android.app.ProgressDialog
import android.os.Bundle
import android.support.v4.app.DialogFragment

/**
 *
 * @author StrangeHare
 *         Date: 25.10.17
 */
class LoadingDialog : DialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = ProgressDialog(activity)
        this.setStyle(STYLE_NO_TITLE, theme)
        dialog.setMessage("Loading..")
        dialog.setCancelable(false)
        return dialog
    }
}