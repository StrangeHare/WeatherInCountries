package com.test.weatherincountries.ui.details

import android.os.Bundle
import android.support.v4.content.ContextCompat
import com.test.weatherincountries.R
import com.test.weatherincountries.ui.base.ActivityBase
import org.jetbrains.anko.appcompat.v7.toolbar
import org.jetbrains.anko.frameLayout
import org.jetbrains.anko.matchParent
import org.jetbrains.anko.verticalLayout
import org.jetbrains.anko.wrapContent

/**
 *
 * @author StrangeHare
 *         Date: 09.10.2017
 */
class ActivityDetails : ActivityBase() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val city = intent.getStringExtra(getString(R.string.city_name))

        verticalLayout {
            toolbar {
                background = ContextCompat.getDrawable(context, R.color.background_toolbar)
                setSupportActionBar(this)
                setNavigationOnClickListener { onBackPressed() }
            }.lparams(width = matchParent, height = wrapContent)

            frameLayout {
                id = R.id.container
                lparams(width = matchParent, height = matchParent)

                supportFragmentManager.beginTransaction().replace(this.id, FragmentDetails.newInstance(city)).commit()
            }
        }
        supportActionBar?.title = city
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }
}
