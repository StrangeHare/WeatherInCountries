package com.test.weatherincountries.ui.details

import android.support.v4.content.ContextCompat
import android.view.Gravity
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.test.weatherincountries.R
import org.jetbrains.anko.*

/**
 *
 * @author StrangeHare
 *         Date: 13.10.17
 */
class DetailFragmentView<in T> : AnkoComponent<T> {

    lateinit var textViewDateFirst: TextView
    lateinit var textViewDateSecond: TextView
    lateinit var textViewDateThird: TextView
    lateinit var textViewDateFourth: TextView
    lateinit var textViewDateFive: TextView
    lateinit var textViewTempFirstMorning: TextView
    lateinit var textViewTempSecondMorning: TextView
    lateinit var textViewTempThirdMorning: TextView
    lateinit var textViewTempFourthMorning: TextView
    lateinit var textViewTempFiveMorning: TextView
    lateinit var imageViewIconFirstMorning: ImageView
    lateinit var imageViewIconSecondMorning: ImageView
    lateinit var imageViewIconThirdMorning: ImageView
    lateinit var imageViewIconFourthMorning: ImageView
    lateinit var imageViewIconFiveMorning: ImageView
    lateinit var textViewTempFirstNight: TextView
    lateinit var textViewTempSecondNight: TextView
    lateinit var textViewTempThirdNight: TextView
    lateinit var textViewTempFourthNight: TextView
    lateinit var textViewTempFiveNight: TextView
    lateinit var imageViewIconFirstNight: ImageView
    lateinit var imageViewIconSecondNight: ImageView
    lateinit var imageViewIconThirdNight: ImageView
    lateinit var imageViewIconFourthNight: ImageView
    lateinit var imageViewIconFiveNight: ImageView

    override fun createView(ui: AnkoContext<T>): View = with(ui) {

        scrollView {
            verticalLayout {
                background = ContextCompat.getDrawable(ui.ctx, R.color.background_item)
                lparams(width = matchParent, height = matchParent)

                textView {
                    id = R.id.text_view_detail_title
                    text = context.getString(R.string.details_title)
                    textSize = 28f
                    gravity = Gravity.CENTER_HORIZONTAL
                }.lparams(width = matchParent, height = wrapContent) {
                    padding = dip(16)
                }

                space {
                }.lparams(width = wrapContent, height = dip(60))

                horizontalScrollView {

                    linearLayout {
                        lparams(width = matchParent, height = matchParent) {
                            bottomMargin = dip(20)
                        }

//                  первый день
                        verticalLayout {
                            lparams(width = matchParent, height = wrapContent) {
                            }

                            textViewDateFirst = textView {
                                id = R.id.text_view_date_first_day
                                textSize = 18f
                            }.lparams(width = matchParent, height = wrapContent) {
                                leftMargin = dip(15)
                            }

                            imageViewIconFirstMorning = imageView {
                                id = R.id.image_view_weather_first_day_morning
                            }.lparams(width = dip(60), height = dip(60))

                            textViewTempFirstMorning = textView {
                                id = R.id.text_view_temp_first_day_morning
                                textSize = 18f
                            }.lparams(width = matchParent, height = wrapContent) {
                                leftMargin = dip(15)
                            }

                            space {
                            }.lparams(width = wrapContent, height = dip(60))

                            imageViewIconFirstNight = imageView {
                                id = R.id.image_view_weather_first_day_night
                            }.lparams(width = dip(60), height = dip(60))

                            textViewTempFirstNight = textView {
                                id = R.id.text_view_temp_first_day_night
                                textSize = 18f
                            }.lparams(width = matchParent, height = wrapContent) {
                                leftMargin = dip(15)
                            }
                        }

//                  второй день
                        verticalLayout {
                            lparams(width = matchParent, height = wrapContent) {
                            }

                            textViewDateSecond = textView {
                                id = R.id.text_view_date_second_day
                                textSize = 18f
                            }.lparams(width = matchParent, height = wrapContent) {
                                leftMargin = dip(15)
                            }

                            imageViewIconSecondMorning = imageView {
                                id = R.id.image_view_weather_second_day_morning
                            }.lparams(width = dip(60), height = dip(60))

                            textViewTempSecondMorning = textView {
                                id = R.id.text_view_temp_second_day_morning
                                textSize = 18f
                            }.lparams(width = matchParent, height = wrapContent) {
                                leftMargin = dip(15)
                            }

                            space {
                            }.lparams(width = wrapContent, height = dip(60))

                            imageViewIconSecondNight = imageView {
                                id = R.id.image_view_weather_second_day_night
                            }.lparams(width = dip(60), height = dip(60))

                            textViewTempSecondNight = textView {
                                id = R.id.text_view_temp_second_day_night
                                textSize = 18f
                            }.lparams(width = matchParent, height = wrapContent) {
                                leftMargin = dip(15)
                            }
                        }

//                  третий день
                        verticalLayout {
                            lparams(width = matchParent, height = wrapContent) {
                            }

                            textViewDateThird = textView {
                                id = R.id.text_view_date_third_day
                                textSize = 18f
                            }.lparams(width = matchParent, height = wrapContent) {
                                leftMargin = dip(15)
                            }

                            imageViewIconThirdMorning = imageView {
                                id = R.id.image_view_weather_third_day_morning
                            }.lparams(width = dip(60), height = dip(60))

                            textViewTempThirdMorning = textView {
                                id = R.id.text_view_temp_third_day_morning
                                textSize = 18f
                            }.lparams(width = matchParent, height = wrapContent) {
                                leftMargin = dip(15)
                            }

                            space {
                            }.lparams(width = wrapContent, height = dip(60))

                            imageViewIconThirdNight = imageView {
                                id = R.id.image_view_weather_third_day_night
                            }.lparams(width = dip(60), height = dip(60))

                            textViewTempThirdNight = textView {
                                id = R.id.text_view_temp_third_day_night
                                textSize = 18f
                            }.lparams(width = matchParent, height = wrapContent) {
                                leftMargin = dip(15)
                            }
                        }

//                  четвертый день
                        verticalLayout {
                            lparams(width = matchParent, height = wrapContent) {
                            }

                            textViewDateFourth = textView {
                                id = R.id.text_view_date_fourth_day
                                textSize = 18f
                            }.lparams(width = matchParent, height = wrapContent) {
                                leftMargin = dip(15)
                            }

                            imageViewIconFourthMorning = imageView {
                                id = R.id.image_view_weather_fourth_day_morning
                            }.lparams(width = dip(60), height = dip(60)) {
                            }

                            textViewTempFourthMorning = textView {
                                id = R.id.text_view_temp_fourth_day_morning
                                textSize = 18f
                            }.lparams(width = matchParent, height = wrapContent) {
                                leftMargin = dip(15)
                            }

                            space {
                            }.lparams(width = wrapContent, height = dip(60))

                            imageViewIconFourthNight = imageView {
                                id = R.id.image_view_weather_fourth_day_night
                            }.lparams(width = dip(60), height = dip(60)) {
                            }

                            textViewTempFourthNight = textView {
                                id = R.id.text_view_temp_fourth_day_night
                                textSize = 18f
                            }.lparams(width = matchParent, height = wrapContent) {
                                leftMargin = dip(15)
                            }
                        }

//                  пятый день
                        verticalLayout {
                            lparams(width = matchParent, height = wrapContent) {
                            }

                            textViewDateFive = textView {
                                id = R.id.text_view_date_five_day
                                textSize = 18f
                            }.lparams(width = matchParent, height = wrapContent) {
                                leftMargin = dip(15)
                            }

                            imageViewIconFiveMorning = imageView {
                                id = R.id.image_view_weather_five_day_morning
                            }.lparams(width = dip(60), height = dip(60)) {
                            }

                            textViewTempFiveMorning = textView {
                                id = R.id.text_view_temp_five_day_morning
                                textSize = 18f
                            }.lparams(width = matchParent, height = wrapContent) {
                                leftMargin = dip(15)
                            }

                            space {
                            }.lparams(width = wrapContent, height = dip(60))

                            imageViewIconFiveNight = imageView {
                                id = R.id.image_view_weather_five_day_night
                            }.lparams(width = dip(60), height = dip(60)) {
                            }

                            textViewTempFiveNight = textView {
                                id = R.id.text_view_temp_five_day_night
                                textSize = 18f
                            }.lparams(width = matchParent, height = wrapContent) {
                                leftMargin = dip(15)
                            }
                        }
                    }
                }.lparams {
                    gravity = Gravity.CENTER_HORIZONTAL
                }
            }
        }
    }
}