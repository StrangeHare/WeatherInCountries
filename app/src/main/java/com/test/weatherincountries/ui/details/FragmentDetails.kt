package com.test.weatherincountries.ui.details

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.squareup.picasso.Picasso
import com.test.weatherincountries.mvp.model.WeatherDay
import com.test.weatherincountries.mvp.presenter.IDetailsPresenter
import com.test.weatherincountries.mvp.view.IDetailsView
import com.test.weatherincountries.ui.base.FragmentBase
import com.test.weatherincountries.ui.base.LoadingDialog
import com.test.weatherincountries.utils.FormatUtils.Companion.format
import org.jetbrains.anko.AnkoContext
import java.util.*
import javax.inject.Inject

/**
 *
 * @author StrangeHare
 *         Date: 17.10.2017
 */
class FragmentDetails : FragmentBase(), IDetailsView {

    @Inject
    lateinit var presenter: IDetailsPresenter<IDetailsView>
    private val fragmentView = DetailFragmentView<Fragment>()
    private val progress: LoadingDialog = LoadingDialog()

    companion object {
        private val ARG_CAUGHT = "CITYNAME"

        fun newInstance(c: String): FragmentDetails {
            val args = Bundle()
            args.putSerializable(ARG_CAUGHT, c)
            val fragment = FragmentDetails()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        progress.show(fragmentManager, LoadingDialog::class.java.simpleName)
        return fragmentView.createView(AnkoContext.create(context, this))
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        fragmentComponent.injectFragmentDetails(this)
        presenter.setCityName(arguments.getString(ARG_CAUGHT))
        presenter.onAttach(this@FragmentDetails)
    }

    override fun showError(error: String?) {
        progress.dismiss()
        Toast.makeText(context, error, Toast.LENGTH_LONG).show()
    }

    override fun onWeatherObtained(listFirstDay: LinkedList<WeatherDay>,
                                   listSecondDay: LinkedList<WeatherDay>,
                                   listThirdDay: LinkedList<WeatherDay>,
                                   listFourDay: LinkedList<WeatherDay>,
                                   listFiveDay: LinkedList<WeatherDay>) {
        setDate(listFirstDay.first.timestamp!! to fragmentView.textViewDateFirst,
                listSecondDay.first.timestamp!! to fragmentView.textViewDateSecond,
                listThirdDay.first.timestamp!! to fragmentView.textViewDateThird,
                listFourDay.first.timestamp!! to fragmentView.textViewDateFourth,
                listFiveDay.first.timestamp!! to fragmentView.textViewDateFive)

        setText(listFirstDay.first.tempWithDegree to fragmentView.textViewTempFirstMorning,
                listSecondDay.first.tempWithDegree to fragmentView.textViewTempSecondMorning,
                listThirdDay.first.tempWithDegree to fragmentView.textViewTempThirdMorning,
                listFourDay.first.tempWithDegree to fragmentView.textViewTempFourthMorning,
                listFiveDay.first.tempWithDegree to fragmentView.textViewTempFiveMorning,
                listFirstDay.last.tempWithDegree to fragmentView.textViewTempFirstNight,
                listSecondDay.last.tempWithDegree to fragmentView.textViewTempSecondNight,
                listThirdDay.last.tempWithDegree to fragmentView.textViewTempThirdNight,
                listFourDay.last.tempWithDegree to fragmentView.textViewTempFourthNight,
                listFiveDay.last.tempWithDegree to fragmentView.textViewTempFiveNight)

        setImage(listFirstDay.first.iconUrl to fragmentView.imageViewIconFirstMorning,
                listSecondDay.first.iconUrl to fragmentView.imageViewIconSecondMorning,
                listThirdDay.first.iconUrl to fragmentView.imageViewIconThirdMorning,
                listFourDay.first.iconUrl to fragmentView.imageViewIconFourthMorning,
                listFiveDay.first.iconUrl to fragmentView.imageViewIconFiveMorning,
                listFirstDay.last.iconUrl to fragmentView.imageViewIconFirstNight,
                listSecondDay.last.iconUrl to fragmentView.imageViewIconSecondNight,
                listThirdDay.last.iconUrl to fragmentView.imageViewIconThirdNight,
                listFourDay.last.iconUrl to fragmentView.imageViewIconFourthNight,
                listFiveDay.last.iconUrl to fragmentView.imageViewIconFiveNight)
        progress.dismiss()
    }

    override fun onDestroyView() {
        presenter.onDetach()
        progress.dismiss()
        super.onDestroyView()
    }

    private fun setText(vararg textPair: Pair<String?, TextView>) {
        for (pair in textPair) {
            pair.second.text = pair.first
        }
    }

    private fun setImage(vararg imagePair: Pair<String?, ImageView>) {
        for (pair in imagePair) {
            Picasso.with(context).load(pair.first).into(pair.second)
        }
    }

    private fun setDate(vararg datePair: Pair<Long, TextView>) {
        for (pair in datePair) {
            pair.second.text = format(pair.first)
        }
    }
}