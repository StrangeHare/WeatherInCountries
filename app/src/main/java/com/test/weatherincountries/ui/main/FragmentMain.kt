package com.test.weatherincountries.ui.main

import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SearchView
import android.util.Log
import android.view.*
import android.widget.Toast
import com.test.weatherincountries.R
import com.test.weatherincountries.mvp.presenter.IMainPresenter
import com.test.weatherincountries.mvp.view.IMainView
import com.test.weatherincountries.ui.adapter.decorator.GridSpacingItemDecoration
import com.test.weatherincountries.ui.base.FragmentBase
import com.test.weatherincountries.ui.base.LoadingDialog
import org.jetbrains.anko.matchParent
import org.jetbrains.anko.recyclerview.v7.recyclerView
import org.jetbrains.anko.support.v4.UI
import org.jetbrains.anko.verticalLayout
import javax.inject.Inject

/**
 *
 * @author StrangeHare
 *         Date: 17.10.2017
 */
class FragmentMain : FragmentBase(), IMainView, SearchView.OnQueryTextListener {

    @Inject
    lateinit var presenter: IMainPresenter<IMainView>
    private lateinit var recycler: RecyclerView
    private val progress: LoadingDialog = LoadingDialog()

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? = UI {
        progress.show(fragmentManager, LoadingDialog::class.java.simpleName)

        verticalLayout {
            recycler = recyclerView {
                layoutManager = GridLayoutManager(context, 2)
                addItemDecoration(GridSpacingItemDecoration(2, 20, true))
            }.lparams(width = matchParent, height = matchParent)
        }
    }.view

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setHasOptionsMenu(true)
        fragmentComponent.injectFragmentMain(this)
        recycler.adapter = presenter.getCurrentAdapter()
        presenter.onAttach(this@FragmentMain)
    }

    override fun showError(error: String?) {
        progress.dismiss()
        Toast.makeText(context, error, Toast.LENGTH_LONG).show()
    }

    override fun onWeatherObtained() {
        Log.d(FragmentMain::class.java.simpleName, getString(R.string.request_success))
        progress.dismiss()
    }

    override fun onDestroyView() {
        presenter.onDetach()
        progress.dismiss()
        super.onDestroyView()
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        menu?.clear()
        inflater?.inflate(R.menu.search_view_menu, menu)
        val item = menu?.findItem(R.id.action_search)
        val searchView = item?.actionView as SearchView
        searchView.setOnQueryTextListener(this)
    }

    override fun onQueryTextSubmit(p0: String?): Boolean = false

    override fun onQueryTextChange(p0: String?): Boolean {
        presenter.filterQuery(p0)
        return true
    }
}