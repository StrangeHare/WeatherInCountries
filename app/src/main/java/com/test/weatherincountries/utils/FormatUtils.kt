package com.test.weatherincountries.utils

import java.text.SimpleDateFormat
import java.util.*

/**
 *
 * @author StrangeHare
 *         Date: 19.10.2017
 */
class FormatUtils {

    companion object {
        fun format(unixTimestamp: Long) = SimpleDateFormat("EE", Locale.getDefault()).format(Date((unixTimestamp * 1000)))!!
    }
}