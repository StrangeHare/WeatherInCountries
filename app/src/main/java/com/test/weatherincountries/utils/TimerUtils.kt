package com.test.weatherincountries.utils

import android.os.CountDownTimer

/**
 *
 * @author StrangeHare
 *         Date: 25.10.2017
 */
class TimerUtils<V> {

    private var timer: CountDownTimer? = null
    var view: V? = null

    fun initTimer(impl: TimerFinish<V>) {
        val callback : TimerFinish<V> = impl
        timer = object : CountDownTimer(30000, 1000) {

            override fun onTick(millisUntilFinished: Long) {
            }

            override fun onFinish() {
                start()
                callback.onFinish(view!!)
            }
        }
    }

    fun attachView(view: V) {
        this.view = view
    }

    fun startTimer() {
        timer?.start()
    }

    fun stopTimer() {
        timer?.cancel()
        timer = null
    }

    interface TimerFinish<V> {

        fun onFinish(view: V)
    }
}